#pragma once

struct Node {
	// Coords of node
	int x;
	int y;

	//// Grid offset from top-left of playfield at which to render
	//int xOffset;
	//int yOffset;

	// Which of the seven shapes is this part of
	int shapeType;

	bool markedForDeletion;
};

