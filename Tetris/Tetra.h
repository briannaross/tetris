#pragma once

#include <SDL.h>
#include "Enums.h"
#include "Node.h"
#include "Stack.h"

class Tetra
{
public:
	Tetra(int nodeType);
	~Tetra();
	void Move(Enums::PlayerMove playerMove);
	bool CollisionDetected(int nodeNum);
	bool IsResting();
	Node& GetNode(int i);
	void MoveTetraOntoStack();
	void Render();

	static void SetRenderer(SDL_Renderer* r);
	static void SetTexture(SDL_Texture* t);
	static void SetStack(Stack* s);

private:
	void MoveByVector(SDL_Point &vector);
	void Rotate();

	Node nodes[4];
	Node testNodes[4];
	SDL_Rect src;
	SDL_Rect dst;
	SDL_Point vector;
	int shapeType;

	// Render offset (each tetra is located around 0,0 but this is not where it should be rendered);
	SDL_Point originOffset;

	// Also needs to be offset from the top-left corner to the screen to the playfield location
	SDL_Point screenOffset;

	int DefaultShapes[7][4][2] =
	{ { { -2,  0 },{ -1,  0 },{  0,  0 },{  1,  0 } }, // I-shape (Cyan)
	  { {  0,  0 },{  0,  1 },{  1,  0 },{  1,  1 } }, // O-shape (Yellow)
	  { {  0,  0 },{  1, -1 },{  1,  0 },{  1,  1 } }, // T-shape (Purple)
	  { { -1, -1 },{  0, -1 },{  0,  0 },{  1,  0 } }, // S-shape (Green)
	  { { -1,  1 },{  0,  0 },{  0,  1 },{  1,  0 } }, // Z-shape (Red)
	  { { -1,  1 },{  0,  1 },{  1,  0 },{  1,  1 } }, // J-Shape (Blue)
	  { { -1, -1 },{  0, -1 },{  1, -1 },{  1,  0 } } }; // L-shape (Orange)

	static SDL_Renderer *renderer;
	static SDL_Texture *texture;
	static Stack *stack;
	static int numTetrasMade;
};
