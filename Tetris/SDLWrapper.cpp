/*
 *
 * Code was originally from Lazy Foo' Productions (http://lazyfoo.net/)
 *
 */

#include <SDL.h>
#include <SDL_image.h>
#include <SDL_ttf.h>
#include <SDL_mixer.h>
#include <stdio.h>
#include <string>
#include <iostream>
#include "GFXData.h"
#include "Globals.h"


bool InitGFX(GFXData &myGFXData)
{
	// Initialise SDL
	if (SDL_Init(SDL_INIT_VIDEO) < 0)
	{
		std::cout << "SDL could not initialize - SDL Error: " << SDL_GetError() << std::endl;
		return false;
	}

	// Create window
	//myGFXData.window = SDL_CreateWindow("My SDL Sandbox", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, SCREEN_WIDTH, SCREEN_HEIGHT, SDL_WINDOW_SHOWN);
	//if (myGFXData.window == NULL)
	//{
	//	std::cout << "Window could not be created - SDL Error: " << SDL_GetError() << std::endl;
	//	return false;
	//}

	if (!myGFXData.window.Init())
	{
		std::cout << "Window could not be created - SDL Error: " << SDL_GetError() << std::endl;
		return false;
	}


	// Initialise PNG loading
	int imgFlags = IMG_INIT_PNG;
	if (!(IMG_Init(imgFlags) & imgFlags))
	{
		std::cout << "SDL_image could not initialize - SDL_image Error: " << IMG_GetError() << std::endl;
		return false;
	}

	// Initialise SDL_ttf
	if (TTF_Init() == -1)
	{
		std::cout << "SDL_ttf could not initialize - SDL_ttf Error: " << TTF_GetError() << std::endl;
		return false;
	}

	// Initialise renderer
	/*myGFXData.renderer = SDL_CreateRenderer(myGFXData.window, -1, 0);
	if (myGFXData.renderer == nullptr)
	{
		std::cout << "Failed to create SDL_Renderer - SDL_Renderer Error: " << SDL_GetError() << std::endl;
		return false;
	}
*/
	myGFXData.renderer = myGFXData.window.CreateRenderer();
	if (myGFXData.renderer == nullptr)
	{
		std::cout << "Failed to create SDL_Renderer - SDL_Renderer Error: " << SDL_GetError() << std::endl;
		return false;
	}

	if (Mix_OpenAudio(44100, MIX_DEFAULT_FORMAT, 2, 2048) < 0)
	{
		std::cout << "Failed to initialise SDL_mixer - SDL_mixer error: " << Mix_GetError() << std::endl;
		return false;
	}

	// If everything initialised return true
	return true;

}

void ShutdownGFX(GFXData &myGFXData)
{
	//Destroy window
	//SDL_DestroyWindow(myGFXData.window);
	//myGFXData.window = NULL;
	myGFXData.window.Free();

	//Quit SDL subsystems
	IMG_Quit();
	SDL_Quit();
}
