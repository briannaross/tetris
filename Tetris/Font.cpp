#include "Font.h"

Font::Font()
{
	mFont = NULL;
	mTextColour = { 0, 0, 0 };
	mTextString = "";
}

Font::~Font()
{
	mTextTexture.Free();
	TTF_CloseFont( mFont );
	mFont = NULL; 
}

bool Font::setFont(GFXData &myGFXData, std::string fontFile)
{
	mFont = TTF_OpenFont(fontFile.c_str(), 24);
	if (mFont == NULL)
	{
		printf("Failed to load lazy font! SDL_ttf Error: %s\n", TTF_GetError());
		return false;
	}

	return true;
}


bool Font::setText(GFXData &myGFXData, std::string text)
{
	if (!mTextTexture.LoadFromRenderedText(mFont, myGFXData, text.c_str(), mTextColour))
	{
		printf("Failed to render text texture!\n");
		return false;
	}

	mTextString = text;
	return true;
}

bool Font::setColour(GFXData &myGFXData, SDL_Color colour)
{
	if (!mTextTexture.LoadFromRenderedText(mFont, myGFXData, mTextString.c_str(), colour))
	{
		printf("Failed to set text colour!\n");
		return false;
	}

	mTextColour = colour;
	return true;
}

SDL_Texture* Font::getTexture()
{
	return mTextTexture.getTexture();
}

int Font::getHeight()
{
	return mTextTexture.getHeight();
}

int Font::getWidth()
{
	return mTextTexture.GetWidth();
}