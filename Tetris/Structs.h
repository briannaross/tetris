#pragma once

#include "Texture.h"
#include "Font.h"

struct Resources
{
	// TODO Investigate using an unordered map so as to make it easier
	// to dynamically create and remove resources. Maybe a map for each
	// resource type, or maybe one big map for everything, don't know yet.
	Texture spriteSheet;
	Texture background;

	Font font;
};
