#include <stdlib.h>
#include <iostream>
#include <time.h>
#include <string>
#include <vector>
#include <list>
#include <random>
#include "SDLWrapper.h"
#include "GFXData.h"
#include "Structs.h"
#include "Texture.h"
#include "Player.h"
#include "Timer.h"
#include "Tetra.h"
#include "Stack.h"

bool InitResources(GFXData &gfxData, Resources &resources);
void RenderScoreboard(GFXData &gfxData, Resources &resources, Player &player);


int main(int argc, char* args[])
{
	GFXData gfxData;
	//std::list<Tetra> tetras;

	// Start up GFX and create window
	if (InitGFX(gfxData))
	{
		/**********************/
		/*** INITIALISATION ***/
		/**********************/

		// Set up resources. If any load fails exit program
		Resources resources;
		if (!InitResources(gfxData, resources))
			exit(1);

		// SDL event handler
		SDL_Event e;
		bool playGame = true;

		Player player;
		player.score = 0;
		player.level = 1;
		player.rowsCleared = 0;

		Stack::SetRenderer(gfxData.renderer);
		Stack::SetTexture(resources.spriteSheet.getTexture());
		Stack stack;

		Tetra::SetRenderer(gfxData.renderer);
		Tetra::SetTexture(resources.spriteSheet.getTexture());
		Tetra::SetStack(&stack);
		
		// Set to number between 1 and 7 for each sprite
		srand((unsigned)time(NULL));
		int tempTetraNumber = rand() % 7;
		Tetra *tetra = new Tetra(tempTetraNumber);
		tempTetraNumber = rand() % 7;
		Tetra *nextTetra = new Tetra(tempTetraNumber);

		Timer fps(60);

		// TODO Modify timer so that it can take in milliseconds. Drop speed needs to be 1/2 to 2/3 what it currently is.
		Timer dropTimer(1);


		while (playGame)
		{
			// Update timers
			fps.Update();
			dropTimer.Update();

			if (fps.HasTicked()) {
				while (SDL_PollEvent(&e) != 0) {
					// User requests quit ('X' in top-right of window on Windows)
					if (e.type == SDL_QUIT)
						playGame = false;
					if (e.type == SDL_KEYDOWN) {
						switch (e.key.keysym.sym) {
							case SDLK_UP:
								tetra->Move(Enums::PlayerMove::UP);
								break;
							case SDLK_DOWN:
								tetra->Move(Enums::PlayerMove::DOWN);
								break;
							case SDLK_LEFT:
								tetra->Move(Enums::PlayerMove::LEFT);
								break;
							case SDLK_RIGHT:
								tetra->Move(Enums::PlayerMove::RIGHT);
								break;
						}
					}
				}
				
				if (dropTimer.HasTicked()) {
					tetra->Move(Enums::PlayerMove::DOWN);
				}

				if (tetra->IsResting()) {
					tetra->MoveTetraOntoStack();
					delete tetra;
					tetra = nextTetra;
					nextTetra = new Tetra(rand() % 7);
					player.score += (4 * player.level);
				}

				int completedRows = stack.CheckForCompletedRows();
				if (completedRows > 0) {
					// Delete those rows!
					stack.DeleteRows();

					// Update player stats
					player.rowsCleared += completedRows;
					player.score += (completedRows * 10 * player.level);

					int oldLevel = player.level;
					player.level = (player.rowsCleared / 10) + 1;
					if (player.level > oldLevel) {
						dropTimer.SetTicksPerSecond(player.level);
					}
				}


				if (stack.TooHigh()) {
					// TODO Change this to a post-game/end-game status
					exit(1);
				}

				/*******************/
				/**** RENDERING ****/
				/*******************/

				// Clear the window
				SDL_RenderClear(gfxData.renderer);

				RenderScoreboard(gfxData, resources, player);

				// Render tetra
				tetra->Render();

				// Render stack
				stack.Render();

				// TODO Render next tetra

				// Render the changes above
				SDL_RenderPresent(gfxData.renderer);
			}
		}

		if (tetra != nullptr) {
			delete tetra;
		}

		if (nextTetra != nullptr) {
			delete nextTetra;
		}

		ShutdownGFX(gfxData);
	}

	return 0;
}

bool InitResources(GFXData &gfxData, Resources &resources)
{
	/*** Textures ***/
	if (!(resources.spriteSheet.LoadFromFile(gfxData, "spriteSheet.png")))
		return false;

	if (!(resources.background.LoadFromFile(gfxData, "background.png")))
		return false;

	/*** Fonts ***/
	SDL_Color white = { 0xFF, 0xFF, 0xFF };
	SDL_Color black = { 0x00, 0x00, 0x00 };
	if (!(resources.font.setFont(gfxData, "ARCADECLASSIC.TTF"))) return false;
	if (!(resources.font.setText(gfxData, "{{Placeholder}}"))) return false;
	if (!(resources.font.setColour(gfxData, black))) return false;

	return true;
}

void RenderScoreboard(GFXData &gfxData, Resources &resources, Player &player)
{
	SDL_Rect dst;
	std::string str;

	dst.x = 0;
	dst.y = 0;
	dst.h = 600;
	dst.w = 400;
	SDL_RenderCopy(gfxData.renderer, resources.background.getTexture(), NULL, &dst);


	SDL_Point screenOffset;
	screenOffset.x = 50;
	screenOffset.y = 50;

	/* Score */
	str = std::to_string(player.score);
	resources.font.setText(gfxData, str);

	dst.x = (int)(270 * gfxData.window.GetSpriteRatio()) + screenOffset.x;
	dst.y = (int)(50 * gfxData.window.GetSpriteRatio()) + screenOffset.y;
	dst.h = (int)(resources.font.getHeight() * gfxData.window.GetSpriteRatio());
	dst.w = (int)(resources.font.getWidth() * gfxData.window.GetSpriteRatio());

	SDL_RenderCopy(gfxData.renderer, resources.font.getTexture(), NULL, &dst);

	// Rows cleared
	str = std::to_string(player.level);
	resources.font.setText(gfxData, str);

	dst.x = (int)(270 * gfxData.window.GetSpriteRatio()) + screenOffset.x;
	dst.y = (int)(175 * gfxData.window.GetSpriteRatio()) + screenOffset.y;
	dst.h = (int)(resources.font.getHeight() * gfxData.window.GetSpriteRatio());
	dst.w = (int)(resources.font.getWidth() * gfxData.window.GetSpriteRatio());

	SDL_RenderCopy(gfxData.renderer, resources.font.getTexture(), NULL, &dst);

	// Rows cleared
	str = std::to_string(player.rowsCleared);
	resources.font.setText(gfxData, str);

	dst.x = (int)(270 * gfxData.window.GetSpriteRatio()) + screenOffset.x;
	dst.y = (int)(300 * gfxData.window.GetSpriteRatio()) + screenOffset.y;
	dst.h = (int)(resources.font.getHeight() * gfxData.window.GetSpriteRatio());
	dst.w = (int)(resources.font.getWidth() * gfxData.window.GetSpriteRatio());

	SDL_RenderCopy(gfxData.renderer, resources.font.getTexture(), NULL, &dst);

}