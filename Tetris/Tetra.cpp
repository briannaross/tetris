#include "Tetra.h"

SDL_Renderer* Tetra::renderer = nullptr;
SDL_Texture* Tetra::texture = nullptr;
Stack* Tetra::stack = nullptr;
int Tetra::numTetrasMade = 0;

Tetra::Tetra(int shapeType)
{
	this->texture = texture;
	this->shapeType = shapeType;

	src.h = 16;
	src.w = 16;
	src.y = 0;
	// TODO Set the y-offsets to their position on the spritesheet
	switch (shapeType) {
		case 0:	src.x = 0;	break;
		case 1:	src.x = 16;	break;
		case 2:	src.x = 32;	break;
		case 3:	src.x = 48;	break;
		case 4:	src.x = 64;	break;
		case 5:	src.x = 80;	break;
		case 6:	src.x = 96;	break;
	}

	dst.h = 16;
	dst.w = 16;

	for (int i = 0; i < 4; i++) {
		nodes[i].x = DefaultShapes[shapeType][i][0];
		nodes[i].y = DefaultShapes[shapeType][i][1];
		//nodes[i].xOffset = 5;
		//nodes[i].yOffset = 5;
		nodes[i].markedForDeletion = false;
		nodes[i].shapeType = shapeType;
	}

	screenOffset.x = 50;
	screenOffset.y = 50;
	originOffset.x = 5;
	originOffset.y = 1;

}

Tetra::~Tetra()
{
}

void Tetra::Move(Enums::PlayerMove playerMove)
{
	bool moveValid = false;
	switch (playerMove) {
	case Enums::LEFT:
		vector = SDL_Point{ -1, 0 };
		MoveByVector(vector);
		break;
	case Enums::RIGHT:
		vector = SDL_Point{ 1, 0 };
		MoveByVector(vector);
		break;
	case Enums::UP:
		Rotate();
		break;
	case Enums::DOWN:
		vector = SDL_Point{ 0, 1 };
		MoveByVector(vector);
		break;
	}
}

void Tetra::MoveByVector(SDL_Point &vector)
{
	// Test to see whether a move is valid.
	for (int i = 0; i < 4; i++) {
		testNodes[i].x = nodes[i].x + vector.x;
		testNodes[i].y = nodes[i].y + vector.y;

		// Move is not valid if there is a collision, so return.
		if (CollisionDetected(i)) {
			return;
		}
	}

	// If we get to this point the move is valid, so move the offset
	// by vector.
	originOffset.x += vector.x;
	originOffset.y += vector.y;
}

void Tetra::Rotate()
{
	// Rotation is around a 0,0 origin. To rotate, x = y, y = -x;
	for (int i = 0; i < 4; i++) {
		// Test the real position in the playfield.
		// Should only have to test for x, because we're only looking to see
		// if we go left or right off the screen.
		testNodes[i].x = nodes[i].y + originOffset.x;
		if (testNodes[i].x < 0 || testNodes[i].x >= 10) {
			return;
		}

		testNodes[i].y = -nodes[i].x + originOffset.y;
		if (testNodes[i].y < 0 || testNodes[i].y >= 22) {
			return;
		}
	}

	for (int i = 0; i < 4; i++) {
		int tmpX = nodes[i].x;
		nodes[i].x = nodes[i].y;
		nodes[i].y = -tmpX;
	}
}

bool Tetra::CollisionDetected(int nodeNum)
{
	// Get the real coords of each node, as the stored coords are
	// based around zero so as to make rotations easy.
	int trueX = testNodes[nodeNum].x + originOffset.x;
	int trueY = testNodes[nodeNum].y + originOffset.y;

	if (trueX < 0 || trueX >= 10) {
		return true;
	}

	if (trueY < 0 || trueY >= 22) {
		return true;
	}

	if (stack->IsNodeAt(trueX, trueY)) {
		return true;
	}

	return false;
}

bool Tetra::IsResting()
{
	for (int i = 0; i < 4; i++) {
		int trueX = nodes[i].x + originOffset.x;
		int trueY = nodes[i].y + originOffset.y;

		if (trueY + 1 >= 22) { // tetra is on playfield floor
			return true;
		}

		if (stack->IsNodeAt(trueX, trueY + 1)) {
			return true;
		}
	}

	return false;
}

Node& Tetra::GetNode(int i)
{
	return nodes[i];
}

void Tetra::MoveTetraOntoStack()
{
	for (int i = 0; i < 4; i++) {
		Node* node = new Node();

		node->x = nodes[i].x + originOffset.x;
		node->y = nodes[i].y + originOffset.y;
		//node->xOffset = nodes[i].xOffset;
		//node->yOffset = nodes[i].yOffset;
		node->shapeType = nodes[i].shapeType;
		node->markedForDeletion = nodes[i].markedForDeletion;

		stack->AddNode(node);
	}
}


void Tetra::Render()
{
	for (int i = 0; i < 4; i++) {
		dst.x = ((nodes[i].x + originOffset.x) * 16) + screenOffset.x;
		dst.y = ((nodes[i].y + originOffset.y) * 16) + screenOffset.y;
		SDL_RenderCopy(renderer, texture, &src, &dst);
	}
}

void Tetra::SetRenderer(SDL_Renderer* r)
{
	renderer = r;
}

void Tetra::SetTexture(SDL_Texture* t)
{
	texture = t;
}

void Tetra::SetStack(Stack* s)
{
	stack = s;
}
