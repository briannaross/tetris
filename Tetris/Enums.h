#pragma once

namespace Enums
{
	enum PlayerMove {
		UP,
		DOWN,
		LEFT,
		RIGHT
	};
}