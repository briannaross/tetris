#pragma once

#include <SDL.h>
#include "Node.h"

class Stack
{
public:
	Stack();
	~Stack();

	bool IsNodeAt(int x, int y);
	int CheckForCompletedRows();
	void AddNode(Node* node);
	void DeleteRows();
	void MoveRowsDown(int startingRow);
	bool TooHigh();
	void Render();

	static void SetRenderer(SDL_Renderer* r);
	static void SetTexture(SDL_Texture* t);

private:

	Node* playfield[22][10];

	SDL_Rect src;
	SDL_Rect dst;

	static SDL_Renderer *renderer;
	static SDL_Texture *texture;

};
