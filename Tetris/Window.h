#pragma once

#include <SDL.h>

class LF_Window
{
public:
	//Intializes internals
	LF_Window();

	//Creates window
	bool Init();

	//Creates renderer from internal window
	SDL_Renderer* CreateRenderer();

	//Handles window events
	void HandleEvent(SDL_Event& e, SDL_Renderer *gRenderer);

	//Deallocates internals
	void Free();

	//Window dimensions
	int GetWidth();
	int GetHeight();
	float GetSpriteRatio();
	int getXBuffer();
	int getYBuffer();
	void SetSpriteRatio();
	SDL_Rect GetWindowRect();

	//Window focii
	bool HasMouseFocus();
	bool HasKeyboardFocus();
	bool IsMinimized();

private:
	//Window data
	SDL_Window* mWindow;

	//Window dimensions
	int mWidth;
	int mHeight;
	int mXBuffer;
	int mYBuffer;
	float mInitScreenRatio;
	float mSpriteRatio;

	//Window focus
	bool mMouseFocus;
	bool mKeyboardFocus;
	bool mFullScreen;
	bool mMinimized;
};
