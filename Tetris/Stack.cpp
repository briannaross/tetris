#include "Stack.h"

SDL_Renderer* Stack::renderer = nullptr;
SDL_Texture* Stack::texture = nullptr;

Stack::Stack()
{
	for (int y = 0; y < 22; y++) {
		for (int x = 0; x < 10; x++) {
			playfield[y][x] = nullptr;
		}
	}

	src.x = 0;
	src.y = 0;
	src.w = 16;
	src.h = 16;
	dst.x = 0;
	dst.y = 0;
	dst.w = 16;
	dst.h = 16;
}

Stack::~Stack()
{
	for (int y = 0; y < 22; y++) {
		for (int x = 0; x < 10; x++) {
			if (playfield != nullptr)
				delete playfield[y][x];
		}
	}
}

bool Stack::IsNodeAt(int x, int y)
{
	if (playfield[y][x] != nullptr) {
		return true;
	}

	return false;
}

int Stack::CheckForCompletedRows()
{
	int completedRows = 0;
	for (int y = 0; y < 22; y++) {
		int occupiedCells = 0;
		for (int x = 0; x < 10; x++) {
			if (playfield[y][x] == nullptr) {
				break;
			}
			occupiedCells++;
		}
		if (occupiedCells == 10) {
			completedRows++;
			for (int x = 0; x < 10; x++) {
				playfield[y][x]->markedForDeletion = true;
			}
		}
	}
	return completedRows;
}

void Stack::AddNode(Node* node)
{
	playfield[node->y][node->x] = node;
}

void Stack::DeleteRows()
{
	for (int y = 21; y >= 0; y--) {
		if (playfield[y][0] != nullptr && playfield[y][0]->markedForDeletion) {
			for (int x = 0; x < 10; x++) {
				delete playfield[y][x];
				playfield[y][x] = nullptr;
			}
			MoveRowsDown(y);
		}
	}
}

void Stack::MoveRowsDown(int startingRow)
{
	for (int y = startingRow; y > 0; y--) {
		for (int x = 0; x < 10; x++) {
			playfield[y][x] = playfield[y - 1][x];

			// Adjust location within node
			if (playfield[y][x] != nullptr) {
				playfield[y][x]->y++;
			}

			playfield[y - 1][x] = nullptr;
		}
	}
}


bool Stack::TooHigh()
{
	for (int x = 0; x < 10; x++) {
		if (playfield[0][x] != nullptr) {
			return true;
		}
	}

	return false;
}

void Stack::Render()
{
	for (int y = 0; y < 22; y++) {
		for (int x = 0; x < 10; x++) {
			if (playfield[y][x] != nullptr)
			{
				// TODO Make a screen offset struct for the....well, screen offsets!
				dst.x = playfield[y][x]->x * dst.w + 50;
				dst.y = playfield[y][x]->y * dst.h + 50;

				if (playfield[y][x]->markedForDeletion) {
					src.x = 0;
					src.y = playfield[y][x]->y * src.h;
				}
				else {
					src.x = src.w * playfield[y][x]->shapeType;
					src.y = 0;
				}

				SDL_RenderCopy(renderer, texture, &src, &dst);
			}
		}
	}
}

void Stack::SetRenderer(SDL_Renderer* r)
{
	renderer = r;
}

void Stack::SetTexture(SDL_Texture* t)
{
	texture = t;
}
